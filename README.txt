Wide Code
==========

Description
-----------

Adds an auto-expanding action on wide code blocks wrapped by _pre_ elements

Installation
------------

1. Download/Install the module
2. You're done!

Configuration
-------------

Not much in the way of configuration. Should just work. You may need to adjust your markup to work with the javascript however (but unlikely).